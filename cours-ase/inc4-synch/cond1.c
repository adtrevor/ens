condition c ;
file *f ;

void *thread_traite (void *arg) {
    if (file_vide(f)) {			// si aucune donnée n'est déjà présente
	cwait (&c) ;			// attendre une donnée
    }
    traiter (extraire_file (f)) ;	// traiter la donnée communiquée
}

void *thread_produit (void *arg) {
    d = lire_donnee (...) ;		// lire une donnée quelque part
    ajouter_file (f, d) ;		// communiquer la donnée au thread de traitement
    csignal (&c) ;			// on a ajouté, on peut réveiller
}
