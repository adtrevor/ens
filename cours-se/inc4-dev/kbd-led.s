// envoyer la commande ED (hexadécimal) au control register
// pour allumer les LED (qui doivent être spécifiées via
// l'input buffer)

    OUT 0x60, 0xed

// attendre que l'input buffer soit vide (donnée d'une
// précédente commande)

boucle:
    IN AL,0x64	    // AL = status register
    TEST AL,0x02    // teste le bit$_1$ du registre AL
    JNZ boucle	    // bit$_1$ $\neq$ 0 $\Rightarrow$ input buffer non vide

// spécifier les LED à allumer via les bits dans l'input buffer
// 0x01 : ScrollLock, 0x02 : NumLock, 0x04 : CapsLock

    OUT 0x60, 0x06  // écrire dans l'input buffer
